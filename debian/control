Source: libcommons-launcher-java
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders: Emmanuel Bourg <ebourg@apache.org>
Build-Depends: debhelper (>= 9), cdbs, ant, default-jdk
Build-Depends-Indep: libcommons-collections3-java, libcommons-logging-java, junit
Standards-Version: 3.9.4
Vcs-Git: https://salsa.debian.org/java-team/libcommons-launcher-java.git
Vcs-Browser: https://salsa.debian.org/java-team/libcommons-launcher-java
Homepage: http://commons.apache.org/launcher/

Package: libcommons-launcher-java
Architecture: all
Depends: libcommons-collections3-java, libcommons-logging-java, ${misc:Depends}
Description: Apache Commons Launcher - Cross platform Java application launcher
 Commons-launcher eliminates the need for a batch or shell script to
 launch a Java class. Some situations where elimination of a batch or
 shell script may be desirable are:
 .
  * You want to avoid having to determining where certain application
    paths are e.g. your application's home directory, etc. Determining this
    dynamically in a Windows batch scripts is very tricky on some versions
    of Windows or when softlinks are used on Unix platforms.
 .
  * You want to avoid having to handle native file and path separators
    or native path quoting issues.
 .
  * You need to enforce certain system properties
    e.g. java.endorsed.dirs when running with JDK 1.4.
 .
  * You want to allow users to pass in custom JVM arguments or system
    properties without having to parse and reorder arguments in your
    script. This can be tricky and/or messy in batch and shell scripts.
 .
  * You want to bootstrap system properties from a configuration file
    instead hard-coding them in your batch and shell scripts.
 .
  * You want to provide localized error messages which is very tricky
    to do in batch and shell scripts.
